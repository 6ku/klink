-- phpMyAdmin SQL Dump
-- version 3.5.3
-- http://www.phpmyadmin.net
--
-- 主机: 10.4.3.92
-- 生成日期: 2013 年 12 月 10 日 13:55
-- 服务器版本: 5.1.72-0ubuntu0.10.04.1
-- PHP 版本: 5.3.2-1ubuntu4.19

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- 数据库: `d266c0314258440ddac89f6c98f1b8999`
--

-- --------------------------------------------------------

--
-- 表的结构 `k_contact`
--

CREATE TABLE IF NOT EXISTS `k_contact` (
  `k_contact_id` int(11) NOT NULL AUTO_INCREMENT,
  `contact_name` varchar(255) NOT NULL,
  `name_py` varchar(20) NOT NULL,
  `group_name` varchar(255) NOT NULL,
  `phone_info` varchar(255) NOT NULL,
  `email_info` varchar(255) NOT NULL,
  PRIMARY KEY (`k_contact_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
