package klink.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;

import com.google.gson.Gson;

public class KLinkDao {

	private JdbcTemplate jdbcTemplate;

	public void setDataSource(DataSource dataSource) {
		jdbcTemplate = new JdbcTemplate(dataSource);
	}

	// 返回库中联系人的数量
	public String getContactCount() {
		Integer rv = this.jdbcTemplate
				.queryForInt("select count(*) as count from k_contact ");
		return "{\"count\" : \"" + rv + "\"}";
	}

	// 根据拼音检索联系人信息;返回json
	public String getInfoByPy(String py) {
		final String group_sql = "select distinct group_name from k_contact where name_py like ? order by group_name";
		final String sql = "select contact_name,email_info,phone_info from k_contact where group_name = ? and name_py like ? order by k_contact_id";

		List<Map<String, Object>> groupList = this.jdbcTemplate.queryForList(
				group_sql, py + "%");
		Map<String, List<Map<String, Object>>> returnMap = new HashMap<String, List<Map<String, Object>>>();

		for (Map<String, Object> groupMap : groupList) {
			List<Map<String, Object>> list = this.jdbcTemplate.queryForList(
					sql, groupMap.get("group_name"), py + "%");
			returnMap.put(groupMap.get("group_name").toString(), list);
		}

		Gson gson = new Gson();
		String jsonData = gson.toJson(returnMap);
		return jsonData;
	}

	// 查询联系人 for panel show
	public String getAllContact() {
		final String group_sql = "select distinct group_name from k_contact order by group_name";
		final String sql = "select k_contact_id,group_name,contact_name,email_info,phone_info from k_contact where group_name = ? order by k_contact_id";

		List<Map<String, Object>> groupList = this.jdbcTemplate
				.queryForList(group_sql);
		Map<String, List<Map<String, Object>>> returnMap = new HashMap<String, List<Map<String, Object>>>();

		for (Map<String, Object> groupMap : groupList) {
			List<Map<String, Object>> list = this.jdbcTemplate.queryForList(
					sql, groupMap.get("group_name"));
			returnMap.put(groupMap.get("group_name").toString(), list);
		}

		Gson gson = new Gson();
		String jsonData = gson.toJson(returnMap);
		return jsonData;
	}

	public String saveContact(String id, String cname, String cgroup,
			String cphone, String cmail) {
		String namePy = getNamePy(cname);
		if (id != null && id.length() > 0) {
			// upd
			final String sql = "update k_contact set contact_name=?,name_py=?,group_name=?,phone_info=?,email_info=? where k_contact_id=?";
			this.jdbcTemplate.update(sql,cname,namePy,cgroup,cphone,cmail,id);
		} else {
			// insert
			final String sql = "insert into k_contact(contact_name, name_py, group_name, phone_info, email_info) values(?,?,?,?,?)";
			this.jdbcTemplate.update(sql,cname,namePy,cgroup,cphone,cmail);
		}
		return "{\"result\" : \"1\"}";
	}

	//获得姓名的首字母
	private static String getNamePy(String name){
		PinYinHelper hanyu = new PinYinHelper();
        String str = name;
        char[] cs = str.toCharArray();
        StringBuilder py = new StringBuilder();
        for(int i=0;i<cs.length;i++){
			if(i == 0 && cs[i] == '单'){
				py.append("s");
			}else if(i == 0 && cs[i] == '解'){
				py.append("x");
			}else if(i == 0 && cs[i] == '仇'){
				py.append("q");
			}else{
				String strPinyin = hanyu.getCharacterPinYin(cs[i]);
				if(strPinyin != null){
					if(strPinyin.length() > 1){
						py.append(strPinyin.substring(0,1));
					}else{
						py.append(strPinyin);
					}
					
				}
			}
        }
        return py.toString();
	}

}
